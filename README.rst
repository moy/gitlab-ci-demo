This is a demo of Gitlab-CI for a simple Python project.

At each push, Gitlab will run flake8 on the Python code, rstcheck on
the documentation, and run the tests.
